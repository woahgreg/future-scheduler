package appt

import "time"

type Appt struct {
	Id        int       `json:"id"`
	TrainerId int       `json:"trainer_id"`
	UserId    int       `json:"user_id"`
	StartedAt time.Time `json:"started_at"`
	EndedAt   time.Time `json:"ended_at"`
}
