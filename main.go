package main

import (
	"log"
	"net/http"

	"github.com/ggrant/scheduler/api"
)

func main() {
	http.HandleFunc("/available", api.AvailableController)
	http.HandleFunc("/new", api.InsertController)
	http.HandleFunc("/scheduled", api.ScheduledController)

	log.Fatal(http.ListenAndServe(":8080", nil))
}
