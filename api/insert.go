package api

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/ggrant/scheduler/appt"
)

func InsertController(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	switch r.Method {
	case "POST":
		w.WriteHeader(http.StatusCreated)

		a, err := insertControllerPost(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		w.Write([]byte(appt.Print([]appt.Appt{*a})))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "Can't find method requested"}`))
	}
}

func insertControllerPost(r *http.Request) (*appt.Appt, error) {
	var query struct {
		TrainerId int       `json:"trainerId"`
		UserId    int       `json:"userId"`
		Start     time.Time `json:"start"`
	}

	err := json.NewDecoder(r.Body).Decode(&query)
	if err != nil {
		return nil, err
	}

	return appt.Insert(query.TrainerId, query.UserId, query.Start)
}
