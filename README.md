**BACKEND ENGINEERING PROJECT**
 
# Appointment Scheduling

### Motivation
At Future, our trainers often schedule video calls with clients to check in on progress and make adjustments. We’d like you to build an API to allow a client to schedule a video call with their trainer.

### Instructions
The client should be able to pick from a list of available times, and appointments for a coach should not overlap.

All appointments are 30 minutes long, and should be scheduled at :00, :30 minutes after the hour during business hours.

Business hours are M-F 8am-5pm Pacific Time

Your goal is to create an HTTP JSON API written in Go with the following endpoints:

• **Get a list of available appointment times for a trainer between two dates:**
Parameters:
```
trainer_id
starts_at
ends_at
```
Returns:
```
list of available appointment times
```

• **Post a new appointment (as JSON) Fields:**
```
trainer_id
user_id
starts_at
```

• **Get a list of scheduled appointments for a trainer:**
Parameters:
```
trainer_id
```

The included file appointments.json contains the current list of appointments in this format:
```
[
 {
  "id": 1
  "trainer_id": 1
  "user_id": 2,
  "starts_at": "2019-01-25T09:00:00-08:00",
  "ends_at": "2019-01-25T09:30:00-08:00"
 }
]
```

You can store appointments in this file, a database or any back end storage you prefer.

## How To Use

* `Time` variables are in the form `2019-01-24T09:00:00-08:00`
* Appointments are stored in the `appointments.json` file at the top level of the project

### Get a list of available appointment times for a trainer between two dates
Type: `GET`

URL: http://localhost:8080/available

Query Parameters:
```
trainerId: Int
start: Time
end: Time
```
Returns list of available appointments during business hours for that trainer.

### Post a new appointment (as JSON)
Type: `POST`

URL: http://localhost:8080/new

Body Parameters:
```
trainerId: Int
userId: Int
start: Time
```
Returns new appointment and appends new appointment to `appointments.json`.

### Get a list of scheduled appointments for a trainer
Type: `GET`

URL: http://localhost:8080/scheduled

Query Parameters:
```
trainerId: Int
```
Returns scheduled appointments for trainer.
