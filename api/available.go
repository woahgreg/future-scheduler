package api

import (
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/ggrant/scheduler/appt"
)

func AvailableController(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusOK)

		appts, err := availableControllerGet(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		w.Write([]byte(appt.Print(appts)))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "Can't find method requested"}`))
	}
}

func availableControllerGet(r *http.Request) ([]appt.Appt, error) {
	q := r.URL.Query()

	// required query params
	if !q.Has("trainerId") || !q.Has("start") || !q.Has("end") {
		return nil, errors.New("missing required parameter(s)")
	}

	trainerId, err := strconv.Atoi(q.Get("trainerId"))
	if err != nil {
		return nil, err
	}

	start, err := time.Parse(time.RFC3339, q.Get("start"))
	if err != nil {
		return nil, err
	}

	end, err := time.Parse(time.RFC3339, q.Get("end"))
	if err != nil {
		return nil, err
	}

	return appt.GetAvailableForTrainer(trainerId, start, end)
}
