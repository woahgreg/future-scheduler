package appt

import (
	"errors"
	"time"
)

// Insert adds a new appt for a trainer with a particular user to json
// returns error if appt already exists for trainer
func Insert(trainerId, userId int, start time.Time) (*Appt, error) {
	if start.Minute() != 0 && start.Minute() != 30 || start.Second() != 0 {
		return nil, errors.New("Appointment must start on the half hour or hour")
	}

	scheduledAppts, err := GetScheduledForTrainer(trainerId)
	if err != nil {
		return nil, err
	}

	// check for existing appt
	for _, a := range scheduledAppts {
		if a.StartedAt == start {
			return nil, errors.New("Appointment unavailable")
		}
	}

	id, err := getNextId()
	if err != nil {
		return nil, err
	}

	if id == nil {
		return nil, errors.New("nil id found")
	}

	a := Appt{
		Id:        *id,
		TrainerId: trainerId,
		UserId:    userId,
		StartedAt: start,
		EndedAt:   start.Add(apptDur),
	}

	err = insertToFile(a)
	if err != nil {
		return nil, err
	}

	return &a, nil
}
