package appt

import (
	"errors"
	"time"
)

// GetAvailableForTrainer returns avaialble appts for a trainer between start
// and end dates
// appts must adhere to the following constraints:
//   30 minutes long
//   monday - friday
//   8am - 5pm pacific
// appts must not already be taken
func GetAvailableForTrainer(
	trainerId int,
	start, end time.Time,
) ([]Appt, error) {
	if end.Before(start) {
		return nil, errors.New("Start time must be before end time")
	}

	scheduledAppts, err := GetScheduledForTrainer(trainerId)
	if err != nil {
		return nil, err
	}

	scheduledApptsByTime := make(map[string]bool)

	// create map of appts by time so we can query for existing time directly
	for _, a := range scheduledAppts {
		scheduledApptsByTime[a.StartedAt.String()] = true
	}

	hour := start.Hour()

	var min int

	// set time to next exact half hour so we can count up from there
	// min is 0 by default so we only need to check between 0 and 30 mins
	if start.Minute() >= 0 && start.Minute() < 30 {
		min = 30 // same hour but at minute 30
	} else {
		hour++ // advance to next hour at 0 minutes
	}

	t := time.Date(
		start.Year(),
		start.Month(),
		start.Day(),
		hour,
		min,
		0, /* second */
		0, /* ns */
		start.Location(),
	)

	loc, err := time.LoadLocation("America/Los_Angeles")
	if err != nil {
		return nil, err
	}

	// convert to pacific time
	t = t.In(loc)

	var availableAppts []Appt

	// add appt duration to start time until one appt duration before end time
	for t.Before(end.Add(-apptDur)) {
		// business hours are M-F 8am-5pm Pacific Time
		// skip all other times
		if t.Weekday() == time.Saturday ||
			t.Weekday() == time.Sunday ||
			t.Hour() < 8 ||
			t.Hour() >= 17 {
			t = t.Add(apptDur)

			continue
		}

		// skip scheduled appts
		if scheduledApptsByTime[t.String()] {

			t = t.Add(apptDur)

			continue
		}

		availableAppts = append(availableAppts, Appt{
			TrainerId: trainerId,
			StartedAt: t,
			EndedAt:   t.Add(apptDur),
		})

		t = t.Add(apptDur)
	}

	return availableAppts, nil
}
