package api

import (
	"errors"
	"net/http"
	"strconv"

	"github.com/ggrant/scheduler/appt"
)

func ScheduledController(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusOK)

		appts, err := scheduledControllerGet(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		w.Write([]byte(appt.Print(appts)))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "Can't find method requested"}`))
	}
}

func scheduledControllerGet(r *http.Request) ([]appt.Appt, error) {
	q := r.URL.Query()

	// required query params
	if !q.Has("trainerId") {
		return nil, errors.New("missing required parameter(s)")
	}

	trainerId, err := strconv.Atoi(q.Get("trainerId"))
	if err != nil {
		return nil, err
	}

	return appt.GetScheduledForTrainer(trainerId)
}
