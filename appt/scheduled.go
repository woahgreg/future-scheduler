package appt

// GetScheduledForTrainer returns all appts for a trainer
func GetScheduledForTrainer(trainerId int) ([]Appt, error) {
	allAppts, err := loadFromFile(path)
	if err != nil {
		return nil, err
	}

	var scheduledAppts []Appt

	for _, a := range allAppts {
		if a.TrainerId == trainerId {
			scheduledAppts = append(scheduledAppts, a)
		}
	}

	return scheduledAppts, nil
}
