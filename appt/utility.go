package appt

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"time"
)

const (
	path    = "appointments.json"
	apptDur = 30 * time.Minute
)

// loadFromFile parses json appts file into and returns []Appt
func loadFromFile(path string) ([]Appt, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	defer file.Close()

	bytes, _ := ioutil.ReadAll(file)

	var appts []Appt

	json.Unmarshal(bytes, &appts)

	return appts, nil
}

// insertToFile appends the new appointment to the end of the json file
func insertToFile(a Appt) error {
	allAppts, err := loadFromFile(path)
	if err != nil {
		return err
	}

	allAppts = append(allAppts, a)

	// write appts to file
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		return err
	}

	fmt.Fprint(f, Print(allAppts))

	// close file
	if err := f.Close(); err != nil {
		return err
	}

	return nil
}

// getNextId returns the next available id by adding one to the max found id
func getNextId() (*int, error) {
	allAppts, err := loadFromFile(path)
	if err != nil {
		return nil, err
	}

	var maxId int

	for _, a := range allAppts {
		if a.Id > maxId {
			maxId = a.Id
		}
	}

	maxId++ // get the next available id

	return &maxId, nil
}

// print formats slice of appts into json string
func Print(appts []Appt) string {
	s := "["

	for i, a := range appts {
		s += "    {\n"

		s += fmt.Sprintf(
			"        \"ended_at\": \"%s\",\n",
			a.EndedAt.Format(time.RFC3339),
		)

		if a.UserId != 0 {
			s += fmt.Sprintf("        \"id\": %d,\n", a.Id)
		}

		if a.UserId != 0 {
			s += fmt.Sprintf("        \"user_id\": %d,\n", a.UserId)
		}

		s += fmt.Sprintf(
			"        \"started_at\": \"%s\",\n",
			a.StartedAt.Format(time.RFC3339),
		)
		s += fmt.Sprintf("        \"trainer_id\": %d\n", a.TrainerId)
		s += "    }"

		if i < len(appts)-1 {
			s += ","
		}

		s += "\n"
	}

	s += "]\n"

	return s
}
